﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace VariablesGlobal
{
    [CreateAssetMenu(fileName = "QuestVariables", menuName = "Custom/GlobalVariables")]
    public class GlobalVariables : ScriptableObject

    {
        public bool TalkedToThatGuy;
    }
}