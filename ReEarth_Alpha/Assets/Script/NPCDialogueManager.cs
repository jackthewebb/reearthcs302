﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using VariablesGlobal;

public class NPCDialogueManager : MonoBehaviour

{
    public Player player;
    public NPC NPCref;
    public Queue<List<string>> sentences;
    public QuestGiver questgiver;
    public DialogueBox ChatBox;
    public InventoryChecker checker;
    public AvatarList avList;

   


    private GlobalVariables globalVariableRef;
   
    string PlayerName = "jeff";
    string NPCname= "jeff";
   
    private int questprogress = 1;

    // list of quests ready to be given out, load quest ids into here when you wan them to be available
    public List<int> availableQuests;
    public enum DialogueType
    {
        Greeting,
        Quest,
        Chatter,
        farewell
    }
    Quest currentQuest;

   void Update()
    {

    }

    void Start()
    {
        
        GetResources();

       

    }

    public void MakeDecision()
    {

        if (NPCref.talk)
        {
            StartTalking();
        }
    }
    void GetResources()
    {

        avList = (AvatarList)Resources.Load("_avatarTest/Avatars");
        globalVariableRef = (GlobalVariables)Resources.Load("QuestVariables");
        ChatBox = FindObjectOfType<DialogueBox>();
        checker = FindObjectOfType<InventoryChecker>();
        player = FindObjectOfType<Player>().GetComponent<Player>();
        NPCref = gameObject.GetComponent<NPC>();
        questgiver = FindObjectOfType<QuestGiver>();

        NPCname = NPCref.NPCname;
        PlayerName = player.playername;

    }
    public void StartTalking()
    {

    }



    public Queue<List<string>> DecideDialogue(int NPCID, int FriendshipLevel, DialogueType DiaMode)
    {

        sentences = new Queue<List<string>>();
        int QuestID = availableQuests[0];
        string text3 = "What's up?";
        string text4 = "Quest";


        StartCoroutine(Converse());

        IEnumerator Converse()
        {

            switch (NPCID)
            {
                ///////////////////////      ANGEL (HAPPY,NORMAL,ANGRY)     ////////////////////
                case 1:
                    switch (DiaMode)
                    {
                        case DialogueType.Greeting:

                            switch (FriendshipLevel)
                            {                                                              
                                case 4:
                                    sentences.Enqueue(new List<string> { "Hi "+ PlayerName, NPCref.NPCname, avList.Angel.emotions.happy.name });
                                    break;
                            }


                            if(player.QuestStatusCheck(1) == 2) { text4 = "Ball"; }
                            
                            StartCoroutine(ChatBox.ChoosingBox(new List<string> { "I am Good", "See you!", text3, text4 }));                           
                            yield return new WaitUntil(() => ChatBox.BroadcastDecision > 0);


                            switch (ChatBox.BroadcastDecision) {
                                case 1:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 2:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.farewell));
                                    break;
                                case 3:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 4:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Quest));
                                    break;

                            }
                            ChatBox.BroadcastDecision = 0;
                            break;
                        case DialogueType.farewell:
                            sentences.Enqueue(new List<string> { "Aww, see ya!", NPCref.NPCname, avList.Angel.emotions.happy.name });
                            break;

                        case DialogueType.Quest:
                         

                            switch (QuestID)
                            {
                                case 1:

                                    switch (player.QuestStatusCheck(1))
                                    {
                                        case 1:
                                        sentences.Enqueue(new List<string> { "What do you want?", NPCref.NPCname, avList.Angel.emotions.angry.name });                                  
                                        sentences.Enqueue(new List<string> { "Is something wrong? You seem annoyed.", PlayerName, avList.Player.emotions.shock.name });
                                        sentences.Enqueue(new List<string> { "Well duh..of course I am! I lost my Dolly playing hide and seek! Wouldn't you be annoyed too?!", NPCref.NPCname, avList.Angel.emotions.angry.name });
                                        sentences.Enqueue(new List<string> { "Oh, I'm sorry. I’d be upset too if I lost my favourite toy. Do you know where it could be?", PlayerName, avList.Player.emotions.shock.name });
                                        sentences.Enqueue(new List<string> { "Weeeeellll, I was playing with her in the forest… but it’s dark now and there's no way I'm going back to get it! ", NPCref.NPCname, avList.Angel.emotions.angry.name });
                                        sentences.Enqueue(new List<string> { "I could go look for you? Do you know where you last saw it?", PlayerName, avList.Player.emotions.normal.name });
                                        sentences.Enqueue(new List<string> { "I dunno but I remember we saw a really pretty flower patch!", NPCref.NPCname, avList.Angel.emotions.angry.name });
                                        sentences.Enqueue(new List<string> { "Well… I guess I will go look around! See you…", PlayerName, avList.Player.emotions.normal.name });




                                            NPCref.questavailable = false;
                                        NPCref.greet = true;
                                        questgiver.AcceptQuest(1);
                                            break;
                                        case 2:
                                            Quest q1 = player.FindQuest(1);
                                            switch (q1.completable)
                                            {
                                                case true:

                                                    sentences.Enqueue(new List<string> { "Wow you found my dolly!", NPCref.NPCname, avList.Angel.emotions.happy.name });
                                                    player.FindQuest(1).completed = true;
                                                    checker.AddtoInv(3, -1);
                                                    removeQuest(1);
                                                    break;
                                                case false:
                                                    sentences.Enqueue(new List<string> { "Have you found me my doll yet?", NPCref.NPCname, avList.Angel.emotions.normal.name });
                                                    sentences.Enqueue(new List<string> { "No sorry i'll keep looking", PlayerName, avList.Player.emotions.normal.name });
                                                    break;
                                               
                                            }
                                            break;                                         
                                    }                                                                                                     
                                    break;                           
                            }

                            break;
                        case DialogueType.Chatter:                                                     
                                    sentences.Enqueue(new List<string> { "I wonder if Leif is busy, I found a really good hide and seek spot!", NPCref.NPCname, avList.Angel.emotions.happy.name });
                          
                            break;
                    }

                    break;              
                case 4:

                    /////////////////////// MOTHER CHARACTER (JUNE) (HAPPY,NORMAL) ////////////////////////
                    switch (DiaMode)
                    {
                        case DialogueType.Greeting:

                            sentences.Enqueue(new List<string> { "Hey there, nice of you to stop by!", NPCref.NPCname, avList.June.emotions.normal.name });

                            StartCoroutine(ChatBox.ChoosingBox(new List<string> { "Chat", "Leave", text3, text4 }));
                            yield return new WaitUntil(() => ChatBox.BroadcastDecision > 0);
                            switch (ChatBox.BroadcastDecision)
                            {
                                case 1:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));

                                    break;
                                case 2:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.farewell));

                                    break;
                                case 3:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break; 
                                case 4:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Quest));
                                    break;                  
                            }
                            ChatBox.BroadcastDecision = 0;

                            break;
                        case DialogueType.farewell:
                            sentences.Enqueue(new List<string> { "This bread isn’t going to knead itself, bye!", NPCref.NPCname, avList.June.emotions.happy.name });
                            break;
                        case DialogueType.Chatter:
                            //Chatter code goes here
                            sentences.Enqueue(new List<string> { "Hmm… did I leave the oven on?", NPCref.NPCname, avList.June.emotions.normal.name });
                            break;


                        case DialogueType.Quest:
                            
                            switch (QuestID)
                            {
                                case 3:


                                    
                                    switch (player.QuestStatusCheck(3))
                                    {
                                        case 1:
                                            sentences.Enqueue(new List<string> { "I made some extra loaves for the people on this list. Think you can handle delivering them for me?", NPCref.NPCname, avList.June.emotions.normal.name });
                                            sentences.Enqueue(new List<string> { "Only if there’s one in there for me!", PlayerName, avList.Player.emotions.normal.name });
                                            sentences.Enqueue(new List<string> { "Of course, what kind of mother would I be without giving you breakfast!", NPCref.NPCname, avList.June.emotions.happy.name });
                                            sentences.Enqueue(new List<string> { "I’ll be off then, thanks!", PlayerName, avList.Player.emotions.happy.name });




                                            questgiver.AcceptQuest(3);

                                            break;
                                        case 2:
                                            switch (player.FindQuest(3).completable)
                                            {
                                                case true:
                                                    sentences.Enqueue(new List<string> { "Thanks a lot for delivering that bread dear", NPCref.NPCname, avList.June.emotions.happy.name });
                                                    player.FindQuest(3).completed = true;
                                                    removeQuest(3);
                                                    break;
                                                case false:
                                                    sentences.Enqueue(new List<string> { "have you delivered the bread yet?", NPCref.NPCname, avList.June.emotions.normal.name });
                                                    sentences.Enqueue(new List<string> { "nope, not yet", PlayerName, avList.Player.emotions.normal.name });
                                                    break;
                                            }
                                            break;
                                    }                               
                                    break;
                                case 99:
                                    sentences.Enqueue(new List<string> { "no chores for you right now dear", NPCref.NPCname, avList.June.emotions.normal.name });
                                    break;
                            }
                            break;
                    }

                    break;

                case 5:
                    /////////////////////// ISANA CHARACTER ////////////////////////
                    switch (DiaMode)
                    {
                        case DialogueType.Greeting:


                            sentences.Enqueue(new List<string> { "What’s up "+PlayerName+"!", NPCref.NPCname, avList.Isana.emotions.normal.name });
                            if (player.QuestStatusCheck(3) == 2)
                            {
                                text3 = "Bread";
                            }
                            StartCoroutine(ChatBox.ChoosingBox(new List<string> { "Chat", "Leave", text3, text4 }));
                            yield return new WaitUntil(() => ChatBox.BroadcastDecision > 0);
                            switch (ChatBox.BroadcastDecision)
                            {
                                case 1:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));

                                    break;
                                case 2:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.farewell));

                                    break;
                                case 3:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 4:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Quest));
                                    break;
                            }
                            ChatBox.BroadcastDecision = 0;

                            break;
                        case DialogueType.Chatter:
                            //Chatter code goes here

                            if (player.QuestStatusCheck(3) == 2) {

                                player.questgiver.questVariables.variables[2] = true;
                                sentences.Enqueue(new List<string> { "Thanks for the bread", NPCref.NPCname, avList.Isana.emotions.happy.name });
                            }
                            else
                            {
                                sentences.Enqueue(new List<string> { "*Giggle* I leave some of my failed tinkering out in the forest for Sage to find, he thinks they’re relics from ages past.", NPCref.NPCname, avList.Isana.emotions.happy.name });
                            }
                            break;
                        case DialogueType.Quest:
                            switch (QuestID)
                            {
                                case 99:
                                    sentences.Enqueue(new List<string> { "I've got no work for ya", NPCref.NPCname, avList.Isana.emotions.normal.name });
                                    break;
                            }
                            break;
                        case DialogueType.farewell:

                            sentences.Enqueue(new List<string> { "Catch ya round!", NPCref.NPCname, avList.Isana.emotions.normal.name });

                            break;
                    }
                    break;
                case 6:
                    /////////////////////////////////////// SAGE (HAPPY,SAD,NORMAL)  (//////////////////////////////////
                    switch (DiaMode)
                    {
                        case DialogueType.Greeting:

                            sentences.Enqueue(new List<string> { "Greetings, child", NPCref.NPCname, avList.Sage.emotions.normal.name });
                            if (player.QuestStatusCheck(3) == 2)
                            {
                                text3 = "Bread";
                            }
                            StartCoroutine(ChatBox.ChoosingBox(new List<string> { "Chat", "Leave", text3, text4 }));
                            yield return new WaitUntil(() => ChatBox.BroadcastDecision > 0);
                            switch (ChatBox.BroadcastDecision)
                            {
                                case 1:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 2:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.farewell));
                                    break;
                                case 3:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 4:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Quest));
                                    break;
                            }
                            ChatBox.BroadcastDecision = 0;

                            break;
                        case DialogueType.farewell:
                            sentences.Enqueue(new List<string> { "Until next we meet!", NPCref.NPCname, avList.Sage.emotions.happy.name });

                            break;
                        case DialogueType.Chatter:
                            //Chatter code goes here
                            if (player.QuestStatusCheck(3) == 2)
                            {

                                player.questgiver.questVariables.variables[3] = true;
                                sentences.Enqueue(new List<string> { "Thanks for the bread", NPCref.NPCname, avList.Sage.emotions.happy.name });
                            }
                            else
                            {
                                sentences.Enqueue(new List<string> { "I find a lot of interesting tech scrap while out foraging, I like to hide it to make it harder for Isana to find!", NPCref.NPCname, avList.Sage.emotions.happy.name });
                            }
                            break;
                        case DialogueType.Quest:
                            switch (QuestID)
                            {

                                case 99:
                                    sentences.Enqueue(new List<string> { "I've got no work for ya", NPCref.NPCname, avList.Sage.emotions.normal.name });
                                    break;
                            }
                            break;
                    }
                    break;
                case 7:
                    /////////////////////// MOZARK (HAPPY,NORMAL) /////////////////////////////////
                    switch (DiaMode)
                    {
                        case DialogueType.Greeting:

                            sentences.Enqueue(new List<string> { "YAHAHAR how are ya "+PlayerName+"?", NPCref.NPCname, avList.Mozark.emotions.happy.name });


                            if (player.QuestStatusCheck(3) == 2)
                            {
                                text3 = "Bread";
                            }
                            if (player.QuestStatusCheck(1) == 2) { text4 = "Wood"; }

                            StartCoroutine(ChatBox.ChoosingBox(new List<string> { "Chat", "Leave", text3, text4 }));
                            yield return new WaitUntil(() => ChatBox.BroadcastDecision > 0);
                            switch (ChatBox.BroadcastDecision)
                            {
                                case 1:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 2:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.farewell));
                                    break;
                                case 3:
                                    //space for dialogue quests or chatter relative to environment
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Chatter));
                                    break;
                                case 4:
                                    ChatBox.ObtainDialogue(DecideDialogue(NPCID, FriendshipLevel, DialogueType.Quest));
                                    break;
                            }
                            ChatBox.BroadcastDecision = 0;

                            break;
                        case DialogueType.farewell:
                            sentences.Enqueue(new List<string> { "Good to see ya "+PlayerName, NPCref.NPCname, avList.Mozark.emotions.happy.name });

                            break;
                        case DialogueType.Chatter:
                            //Chatter code goes here

                            if (player.QuestStatusCheck(3) == 2)
                            {

                                player.questgiver.questVariables.variables[4] = true;
                                sentences.Enqueue(new List<string> { "Thanks for the bread", NPCref.NPCname, avList.Mozark.emotions.happy.name });
                            }
                            else
                            {
                                sentences.Enqueue(new List<string> { "Nothing beats the smell of fresh hot steel! Well maybe the smell of fresh hot bread...", NPCref.NPCname, avList.Mozark.emotions.normal.name });
                            }
                            break;
                        case DialogueType.Quest:
                            switch (QuestID)
                            {
                                case 4:
                                    switch (player.QuestStatusCheck(4))
                                    {
                                        case 1:
                                            sentences.Enqueue(new List<string> { "I’m needin’ some wood for my forge. I’d go myself but I’ve been backed up on work for this darn festival.", NPCref.NPCname, avList.Mozark.emotions.normal.name });
                                            sentences.Enqueue(new List<string> { "I can take care of it!", PlayerName, avList.Player.emotions.normal.name });
                                            sentences.Enqueue(new List<string> { "YARHAHAHAR, I knew I could count on ya!", NPCref.NPCname, avList.Mozark.emotions.happy.name });

                                            questgiver.AcceptQuest(4);

                                            break;
                                        case 2:
                                            switch (player.FindQuest(4).completable)
                                            {
                                                case true:
                                                    sentences.Enqueue(new List<string> { "Wow you did it, thanks a bunch. ", NPCref.NPCname, avList.Mozark.emotions.happy.name });
                                                    player.FindQuest(4).completed = true;
                                                    removeQuest(4);
                                                    break;
                                                case false:
                                                    sentences.Enqueue(new List<string> { "Have ya fetched me the wood yet?", NPCref.NPCname, avList.Mozark.emotions.normal.name });
                                                    sentences.Enqueue(new List<string> { "nope, not yet", PlayerName, avList.Player.emotions.normal.name });
                                                    break;
                                            }
                                            break;
                                    }
                                                                       
                                    break;
                            }
                            break;
                    }

                    break;

            }
            yield return null;
        }

        return sentences;
    }
    public int CurrentQuest()
    {
        Quest quest = null;

        foreach (int i in availableQuests)
        {
            quest =  player.FindQuest(i);
        }
        currentQuest = quest;
        return quest.ID;
    }


    void removeQuest(int quest)
    {
        foreach(int q in availableQuests)
        {
            if(q == quest)
            {
                availableQuests.Remove(q);
            }
        }
    }


}
