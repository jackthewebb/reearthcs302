﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using TestInventory;


public class Pickup : MonoBehaviour
{


    public int itemID = 0;
    public int amount = 1;
    public string Name = "NoName";
    public string description = "None";


    public bool glowing = false;

    private Inventory inventoryRef;
    public InventoryChecker checker;




    void Start()
    {

        checker = FindObjectOfType<InventoryChecker>();
        GetResources();


        
    }
    void GetResources()
    {
        inventoryRef = (Inventory)Resources.Load("Inventories/Inventory");


    }

    void Update()
    {
       
       
    }
    
    public void glow()
    {
            if(glowing == true){
        GetComponent<Renderer>().material.color = new Color(0, 255, 0);			
     } 
        else {
      	//GetComponent<Renderer>().material.color = DefaultColour;		
       }



    }


    public void AddInv()
    {
        checker.AddtoInv(itemID, amount);
    }
}
