﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectFinder : MonoBehaviour
{

    GameObject newobject;
    GameObject newRock;
   
    public string playername = "Jack";
    public NPC npcRef;
    public bool talking = false;   
    public Hands handsref;
    public rock[] allrocks;
    public InteractableObject[] allOBJs;
    public GameObject newobj;
    public DialogueBox chatboxref;
    public List<GameObject> ObjInRng = new List<GameObject>();
    public List<GameObject> RocksinRange = new List<GameObject>();
    public List<GameObject> NPCsinRange = new List<GameObject>();
    public Quest quest;

    private void Start()
    {

        handsref = FindObjectOfType<Hands>();
        chatboxref = FindObjectOfType<DialogueBox>();

    }

    private void OnLevelWasLoaded(int level)
    {
        handsref = FindObjectOfType<Hands>();
        chatboxref = FindObjectOfType<DialogueBox>();
    }

    void Update() {

        if (Input.GetKeyDown("q"))
        {
            
            allrocks = FindObjectsOfType<rock>();            
            foreach(rock r in allrocks)
            {
                if(r.inrange == true)
                {
                    if (r.type == 1)
                    {
                    if (handsref.InHandItem.ToolType == 1)
                    {
                    r.Mine();
                    }
                    else
                    {

                        Debug.Log("YOU NEED A PICKAXE TO MINE THIS");

                    }

                    }
                    if (r.type == 2)
                    {
                        if (handsref.InHandItem.ToolType == 2)
                        {
                            r.Mine();
                        }
                        else
                        {
                            Debug.Log("YOU NEED An AXE TO MINE THIS");
                        }
                    }
                }
            }
        }



            if (Input.GetKeyDown("space")) {

            if (NPCsinRange.Count > 0 && chatboxref.talking == false)
            {
                NPCsinRange[0].GetComponent<NPC>().StartDialogue();
                NPCsinRange.Clear();
            }

            allOBJs = FindObjectsOfType<InteractableObject>();
            foreach(InteractableObject i in allOBJs)
                {
                if (i.inrange)
                {
                    StartCoroutine(i.interact());
                }
               

            }




            for (int i = 0; i < ObjInRng.Count; i++) {

                ObjInRng[i].GetComponent<Pickup>().AddInv();
                ObjInRng[i].transform.position = new Vector3(0f, -10f, 0f);
                ObjInRng[i].GetComponent<Rigidbody>().isKinematic = true;

               
            }
        }


    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Interactable")) {
            
            newobject = other.gameObject;
            ObjInRng.Add(other.gameObject);
            other.GetComponent<Pickup>().glowing = true;

        }

        if (other.gameObject.CompareTag("NPC")) {
            newobject = other.gameObject;
            npcRef = other.GetComponent<NPC>();
            NPCsinRange.Add(other.gameObject);


        }
		if (other.gameObject.CompareTag("door")) {
           other.GetComponent<Door1>().GoThoughDoor();

		   
        }
        if (other.gameObject.CompareTag("Minable"))
        {
            newRock = other.gameObject;
            newRock.gameObject.GetComponent<rock>().inrange = true;

        }
        if (other.gameObject.CompareTag("MapInteraction"))
        {
            newobj = other.gameObject;
            newobj.gameObject.GetComponent<InteractableObject>().inrange = true;


        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Interactable")) {
            ObjInRng.Clear();
			other.GetComponent<Pickup>().glowing = false;
        }
        if (other.gameObject.CompareTag("Minable"))
        {
            newRock = other.gameObject;
            newRock.gameObject.GetComponent<rock>().inrange = false;



        }
        if (other.gameObject.CompareTag("NPC"))
            {

            NPCsinRange.Clear();

            }
        if (other.gameObject.CompareTag("MapInteraction"))
        {

            newobj = other.gameObject;
            newobj.gameObject.GetComponent<InteractableObject>().inrange = false;

        }

    }
        


}