﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class QuestlogUI : MonoBehaviour
{
    public GameObject QuestlogElement;
    private GameObject QuestInfo;
    public Player player;
    public Text title, description;
    public GameObject GoalsGrid;
    public GameObject GoalElement;
    public CanvasGroup questdata;

    private void Start()
    {
        player = FindObjectOfType<Player>();
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void CreateQuestInfo(Quest quest)
    {
        QuestInfo = Instantiate(QuestlogElement, gameObject.transform);
        QuestInfo.GetComponent<QuestlogElement>().quest = quest;
        QuestInfo.GetComponent<QuestlogElement>().myparent = this;



    }
    void destroygoals()
    {
        GoalElement[] destroyed2 = FindObjectsOfType<GoalElement>();

        foreach (GoalElement g in destroyed2)
        {
            Destroy(g.gameObject);
        }
    }
    public void destroyall()
    {
        QuestlogElement[] destroyed = GetComponentsInChildren<QuestlogElement>();
        destroygoals();

        foreach (QuestlogElement g in destroyed)
        {
            Destroy(g.gameObject);
        }
        HideQuestData();
    }

    public void DisplayQuestData(Quest quest)
    {
        destroygoals();
        questdata.alpha = 1;
        title.text = quest.title;
        description.text = quest.desc;
        if (quest.goals.Count > 1)
        {
            foreach (QuestGoal g in quest.goals)
            {

            GameObject goal = Instantiate(GoalElement, GoalsGrid.transform);
                goal.GetComponent<GoalElement>().text.text = g.description;
                if (g.completed)
                {
                goal.GetComponent<GoalElement>().text.fontStyle = FontStyles.Strikethrough;

                }

            }

        }

    }
    public void HideQuestData()
    {
        title.text = "";
        description.text = "";
    }
}
