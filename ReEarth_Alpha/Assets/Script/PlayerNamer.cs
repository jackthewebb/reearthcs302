﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerNamer : MonoBehaviour
{
    public InputField input;
    public Text Title;
    private void Start()
    {
        StartCoroutine(AnimateText("What is your name?"));
    }

    public void namer()
    {

        Player player = FindObjectOfType<Player>();
        string name = input.text;
        player.GetComponent<Player>().playername = name;
        SceneManager.LoadScene("Main");

    }
    private void Update()
    {
        if (Input.GetKeyDown("return"))
        {
            namer();

        }
    }

    IEnumerator AnimateText(string text)
    {
        yield return new WaitForSeconds(.5f);

        for (int i = 0; i < (text.Length + 1); i++)
        {
            Title.text = text.Substring(0, i);
            yield return new WaitForSeconds(.1f);

        }

    }





}
