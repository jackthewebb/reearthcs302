﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Hands : MonoBehaviour
{


    public Tool[] tools;
    public Tool InHandItem;
    public PlayerMovement player;
    public Image ToolImage;
    public Sprite pickicon;
    public Sprite axeicon;
    public Sprite fishingrodicon;
    public Sprite nothingicon;


    private void Start()
    {
        ToolImage = FindObjectOfType<ToolsUI>().GetComponent<Image>();
        player = FindObjectOfType<PlayerMovement>();

    }
    private void OnLevelWasLoaded(int level)
    {
        ToolImage = FindObjectOfType<ToolsUI>().GetComponent<Image>();
        player = FindObjectOfType<PlayerMovement>();
    }

    void Update()
    {

        HotbarSlots();

    }

    
    IEnumerator SwapToItem(int NumberPressed)

    {
        tools = FindObjectsOfType<Tool>();

        Debug.Log("swapping" + 2);
        foreach (Tool x in tools)
        {
            if (x.toolnum == NumberPressed)
            {
                InHandItem.visibility = false;
                x.visibility = true;
                InHandItem = x;
                switch (NumberPressed)
                {
                    case 1:
                        ToolImage.sprite = pickicon;
                        break;
                    case 2:
                        ToolImage.sprite = axeicon;
                        break;
                    case 3:
                        ToolImage.sprite = nothingicon;
                        break;
                    case 4:
                        ToolImage.sprite = nothingicon;
                        break;


                }
                yield return null;


            }
            yield return null;

        }
    }

    void HotbarSlots()

    {
        if (player.canmove)
        {

            if (Input.GetKeyDown("1"))
            {
                StartCoroutine(SwapToItem(1));

            }
            ///
            if (Input.GetKeyDown("2"))
            {
                StartCoroutine(SwapToItem(2));
            }
            ///
            if (Input.GetKeyDown("3"))
            {
                StartCoroutine(SwapToItem(3));
            }
            ///
            if (Input.GetKeyDown("4"))
            {
                StartCoroutine(SwapToItem(4));
            }
            ///
            if (Input.GetKeyDown("5"))
            {
                StartCoroutine(SwapToItem(5));
            }
            ///
            if (Input.GetKeyDown("6"))
            {
                StartCoroutine(SwapToItem(6));
            }
            ///
            if (Input.GetKeyDown("7"))
            {
                StartCoroutine(SwapToItem(7));
            }
            if (Input.GetKeyDown("0"))
            {
                StartCoroutine(SwapToItem(8));
            }
            ///
        }
    }





}










