﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackpackElement : MonoBehaviour
{
    public int id;
    public string name;
    public int quantity;
    
    public Text nameText;
    public Text QuanText;

    private void Start()
    {
        nameText.text = name.ToString();
        QuanText.text = quantity.ToString();
    }
}
