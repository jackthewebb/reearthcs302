﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class NPC : MonoBehaviour
{

    public string playername;
    public string NPCname;
    public int freindshipLevel = 1;
    public int id;
    public Queue<string> sentences;
    public DialogueBox ChatBoxRef;
    public int[] RelativeQuests = { 1, 2, 3 };
    public bool greet;
    public bool talking;
    public bool questavailable;
    public bool questinprogress;
    public bool chatter;
    public bool talk;
    public NPCDialogueManager DialogueManager;
    public Quest quest;


    public enum NPCid
    {
        SmallBoy,
        Australian,
    }

    void Start() {

        DialogueManager = gameObject.GetComponent<NPCDialogueManager>();

        playername = FindObjectOfType<objectFinder>().playername;
        sentences = new Queue<string>();
        ChatBoxRef = FindObjectOfType<DialogueBox>();

    }

    public void StartDialogue()
    {

        DialogueManager.MakeDecision();
        if (talk && greet)
        {

            ChatBoxRef.ShowBox();

            ChatBoxRef.ObtainDialogue(DialogueManager.DecideDialogue(id, freindshipLevel, NPCDialogueManager.DialogueType.Greeting));

        }

        if (talk && questavailable)
        {
            ChatBoxRef.ObtainDialogue(DialogueManager.DecideDialogue(id, freindshipLevel, NPCDialogueManager.DialogueType.Quest));

        }
        if (talk && chatter)
        {
            ChatBoxRef.ObtainDialogue(DialogueManager.DecideDialogue(id, freindshipLevel, NPCDialogueManager.DialogueType.Chatter));
        }

        ChatBoxRef.StartTalking();

    }


}
 
      