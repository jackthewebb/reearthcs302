﻿using UnityEngine;

[ExecuteInEditMode]
public class DayNightCycleManager : MonoBehaviour {
    public Transform sun, moon;
    public bool isPaused;

    [SerializeField] int distanceFromOrigin = 10;
    [SerializeField] float daySpeed = .1f;

    [Range(0, 24)]
    public float timeOfDay;

    void Start() {
        SetUpOrbitals();
    }

    void SetUpOrbitals() {
        Vector3 distanceFromOrginVector = new Vector3(distanceFromOrigin, 0, 0);

        sun.position = distanceFromOrginVector;
        moon.position = -distanceFromOrginVector;

        if (isPaused) return;

        sun.rotation = Quaternion.Euler(0, -90, 0);
        moon.rotation = Quaternion.Euler(0, 90, 0);
    }

    void Update() {
        //transform.Rotate(0, 0, daySpeed);

        if (Application.isPlaying & !isPaused) {
            timeOfDay += daySpeed * Time.deltaTime;
        }

        if (timeOfDay > 24 && Application.isPlaying) {
            timeOfDay = 0;
        }

        float rotation = Mathf.Lerp(0, 360, timeOfDay / 24);
        transform.localEulerAngles = new Vector3(0, 0, rotation);
    }
}
