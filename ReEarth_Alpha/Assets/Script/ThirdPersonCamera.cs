﻿using UnityEngine;

[ExecuteInEditMode]
public class ThirdPersonCamera : MonoBehaviour {
    public static ThirdPersonCamera Instance;

    public PlayerMovement target;

    public float mouseSensitivity = 5;
    public float zoomSensitivity = 2;
    public Vector2 minMaxPitch = new Vector2(10, 80);
    public Vector2 minMaxZoom = new Vector2(5, 15);
    public bool CameraCanMove;

    private float yaw;
    private float pitch;
    private float zoom;
    private Camera mainCamera;
    public bool canrotate = true;
    MenuManager menuref;

    private void Awake() {
        Instance = this;

        if (Application.isPlaying) {
            //Cursor.lockState = CursorLockMode.Locked;
        }

        mainCamera = Camera.main;

        Instance = this;
    }
    void Start()
    {

        target = FindObjectOfType<PlayerMovement>();
        menuref = FindObjectOfType<MenuManager>(); 

    }

    private void Update() {

        if (canrotate)
        {
            yaw += Input.GetAxis("Mouse X") * mouseSensitivity;

            pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            pitch = Mathf.Clamp(pitch, minMaxPitch.x, minMaxPitch.y);

            zoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
            zoom = Mathf.Clamp(zoom, minMaxZoom.x, minMaxZoom.y);
        }
    }

    private void LateUpdate() {
        if (!target) return;

        transform.position = target.transform.position;

        transform.localEulerAngles = new Vector3(0, yaw, 0);
        transform.GetChild(0).localEulerAngles = new Vector3(pitch, 0, 0);
        mainCamera.transform.localPosition = Vector3.forward * -zoom;
    }

}
