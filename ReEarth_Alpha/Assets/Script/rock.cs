﻿using System.Collections;
using System.Collections.Generic;
using TestInventory;
using UnityEngine;

public class rock : MonoBehaviour
{
    public bool inrange = false;
    public int Health = 100;
    public int type = 1;
    public Color defaultColor;
    public Pickup rocktodrop;
    public InventoryChecker inventory;

    private Inventory inventoryRef;

    void Start()
    {
        defaultColor = gameObject.GetComponent<MeshRenderer>().material.color;
        inventory = FindObjectOfType<InventoryChecker>();
        GetResources();
    }
        
        void GetResources() {
        inventoryRef = (Inventory)Resources.Load("Inventories/Inventory");
    }
     
    void Update()
    {


        switch (inrange)
        {
            case true:
                GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                break;
            case false:
                GetComponent<Renderer>().material.color = defaultColor;

                break;

        }
        
        if(Health <= 0)
        {
            switch (type)
            {
                case 1:
                    inventory.AddtoInv(1, 3);

                    break;
                case 2:
                    inventory.AddtoInv(2, 3);
                    break;
            }

            Destroy(gameObject);
        }
    }

    public void Mine()
    {

        Health -= 30;
        StartCoroutine(FlashRed());

       
    }
    IEnumerator FlashRed()
    {
        GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        yield return new WaitForSeconds(1);
        GetComponent<Renderer>().material.color = new Color(0, 255, 0);
        


        yield return null;
    }




    }
