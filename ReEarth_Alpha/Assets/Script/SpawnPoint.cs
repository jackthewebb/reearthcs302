﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnPoint : MonoBehaviour {
    public Player player;
    public int ID;
    private bool spawning;

    private Transform destination;

    public GameObject newplayer;



    // Start is called before the first frame update
    void Start() {


        destination = transform;
        player = FindObjectOfType<Player>();
        StartCoroutine(Spawn());
                Debug.Log(SceneManager.GetActiveScene().name);
        if (player == null)
        {
            Instantiate(newplayer);
            player = FindObjectOfType<Player>();
            player.spawn();

        }
    }

  

    void Update() {
        player = FindObjectOfType<Player>();

        if (player.doorTicket == ID)
        {

            if (spawning)
            {
                player.spawn();
            }
        }

    }


    public IEnumerator Spawn() {
        spawning = true;
        yield return new WaitForSeconds(1);  
        spawning = false;
      
    }
}
