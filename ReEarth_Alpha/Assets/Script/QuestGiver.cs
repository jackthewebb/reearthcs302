﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class QuestGiver : MonoBehaviour
{
    public GlobalQuestVariables questVariables;
    public Player player;
    public List<Quest> ListOfQuests;
    public void AcceptQuest(int QuestID)
    {


        player = FindObjectOfType<Player>();
        player.ActiveQuests.Insert(0, ListOfQuests[QuestID]);

    }

    private void Start()
    {

        player = FindObjectOfType<Player>();

    }



    private void OnLevelWasLoaded(int level)
    {
        player = FindObjectOfType<Player>();

    }


}

