﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Namer : MonoBehaviour
{
    
   
    private Text mesh;
    public objectFinder player;
    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<Text>();
        player = FindObjectOfType<objectFinder>();

    }

    // Update is called once per frame
    void Update()
    {
        if(mesh.text == "SampleScene")
        {
            mesh.text = player.playername;
        }
    }
}
