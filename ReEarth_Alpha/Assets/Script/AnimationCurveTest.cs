﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class AnimationCurveTest : MonoBehaviour {
    public AnimationCurve curve;
    [Range(0, 1)]
    public float sample;
    public float output;
    public Color c;

    void Update() {
        output = curve.Evaluate(sample);

        c = Color.Lerp(Color.red, new Color(1, 1, 0, 1), output);
    }
}
