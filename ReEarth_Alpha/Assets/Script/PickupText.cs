﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupText : MonoBehaviour
{  
  public void DisplayText(int amount, string item)
    {
        IEnumerator displayer()
        {
        gameObject.GetComponent<Text>().text = ("+ " + amount.ToString() + "  " + item);
            yield return new WaitForSeconds(2);
            Destroy(gameObject);
        }
        StartCoroutine(displayer());
    }
}
