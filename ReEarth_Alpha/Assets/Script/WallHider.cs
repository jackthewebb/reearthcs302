﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallHider : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("wall"))
        {

           other.gameObject.GetComponent<MeshRenderer>().enabled = false;

        }       
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("wall"))
        {

            other.gameObject.GetComponent<MeshRenderer>().enabled = true;


        }
    }
}
