﻿using UnityEngine;

[ExecuteAlways]
public class LightingManager : MonoBehaviour {
    //References
    [SerializeField]
    private Light sun;
    [SerializeField]
    private Light moon;
    [SerializeField]
    private LightingPreset preset;

    //Variables
    [SerializeField, Range(0, 24)]
    private float timeOfDay;

    private void Update() {
        if (preset == null)
            return;
        if (Application.isPlaying) {
            timeOfDay += Time.deltaTime;
            timeOfDay %= 24; //Clamped between 0-24
            UpdateLighting(timeOfDay / 24f);
        } else {
            UpdateLighting(timeOfDay / 24f);
        }
    }

    private void UpdateLighting(float timePercent) {
        RenderSettings.ambientLight = preset.AmbientColor.Evaluate(timePercent);
        RenderSettings.fogColor = preset.FogColor.Evaluate(timePercent);
        //RenderSettings.fogColor = Color.blue;
        if (sun != null) {
            sun.color = preset.SunColor.Evaluate(timePercent);
            //sun.transform.rotation = Quaternion.Euler(new Vector3((timePercent * 360f) - 90f, 170f, 0));
            transform.localEulerAngles = new Vector3((timePercent * 360f) - 90f, 0, 0);
        } 
        
        if (moon != null) {
            moon.color = preset.MoonColor.Evaluate(timePercent);
        }
    }
}