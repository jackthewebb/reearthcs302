﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private Transform nextspawnpoint;
    public List<Quest> ActiveQuests;
    public List<Quest> FinishedQuests;
    public InventoryChecker checker;
    public Text text1;
    public Text text2;
    public int doorTicket;
    public BackpackUI BPUI;
    public QuestlogUI QuestUI;
    public bool showd;
    public string playername = "jeff";
    public ThirdPersonCamera camRef;
    PlayerMovement playerM;
    public MenuManager menus;
    public bool canMenu = true;
    public QuestGiver questgiver;
    private void Start()
    {
        Getrefs();

 
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLoad;
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnLoad;
    }

    private void OnLoad(Scene _scene, LoadSceneMode _loadSceneMode)
    {
        Getrefs();
        StartCoroutine(spawn());
            ; 

        playerM = FindObjectOfType<PlayerMovement>();
    }




    void Getrefs()
    {
        questgiver = FindObjectOfType<QuestGiver>();
        QuestUI = FindObjectOfType<QuestlogUI>();
        camRef = FindObjectOfType<ThirdPersonCamera>();
        checker = FindObjectOfType<InventoryChecker>();
        BPUI = FindObjectOfType<BackpackUI>();
        menus = FindObjectOfType<MenuManager>();
        playerM = gameObject.GetComponent<PlayerMovement>();
        SpawnPoint[] spawnPoints;
        spawnPoints = FindObjectsOfType<SpawnPoint>();
        foreach (SpawnPoint s in spawnPoints)
        {
            if (s.ID == doorTicket)
            {
                nextspawnpoint = s.transform;
            }
        }
        if (nextspawnpoint == null)
        {
            nextspawnpoint = spawnPoints[0].transform;
        }

    }
    void DisplayInventory()
    {
        
            if (!menus.Open)
            {
                menus.OpenMenu();
            }
            else
            {
                menus.CloseMenu();
            }
        
    }

    public void EnterGameplayState() {

        camRef.canrotate = true;
        playerM.canmove = true;


    }
    public void ExitGameplayState()
    {

        camRef.canrotate = false;
        playerM.canmove = false;


    }





    private void Update()
    {
   
  
        if(Input.GetKeyDown("tab") && canMenu)
        {
            DisplayInventory();
          
        }
       


        if (ActiveQuests.Count > 0)
        {
            UpdateQuests();

        }

    }
    public void UpdateQuests()
    {
            foreach (Quest q in ActiveQuests)
            {
            if (q.completed)
            {
                FinishedQuests.Add(q);
                ActiveQuests.Remove(q);
            }

            else
            {

                foreach (QuestGoal g in q.goals)
                {
                    switch (g.questtype)
                    {
                        case QuestGoal.QuestType.Gather:
                    int requireditem = g.requireditem;
                    int requiredAmount = g.requiredAmount;
                    g.completed = checker.GatherQuestCheck(requireditem, requiredAmount);
                            break;
                        case QuestGoal.QuestType.Dialogue:
                            
                            if (questgiver.questVariables.variables[g.requiredBool])
                            {
                                g.completed = true;
                            }
                            break;
                    }
                }
                q.comepletequest();

            }
        }
          
    }
    public IEnumerator spawn()
    {
        Debug.Log("SPAWNING");
        yield return null;


        transform.position = nextspawnpoint.position;


    }
    public Quest FindQuest(int QuestID)
    {
        Quest newquest = null;
        foreach(Quest q in ActiveQuests)
        {
            if (QuestID == q.ID)
            {
                newquest = q;
                return newquest;

            }
            else
            {

            }
        }
        return newquest;
    }


    public int QuestStatusCheck(int QuestID)
    {
        //STATUS, 1 = not found, 2 = in progress, 3 = completed

       
        int status = 1;

        foreach (Quest q in ActiveQuests)
        {
            if (QuestID == q.ID)
            {
                status = 2;
            }
        }
        foreach (Quest q in FinishedQuests)
        {
            if (QuestID == q.ID)
            {
                status = 3;
            }
        }

        return status;

    }




    public void QuestLogDisplayer()
    {
        List<Quest> QuestsToShow = ActiveQuests;

        foreach (Quest i in QuestsToShow)
        {
            QuestUI.CreateQuestInfo(i);

        }

    }







}



