﻿using System;
using System.Collections.Generic;

[Serializable]
public class Quest {

    public bool active;
    public bool completed;
    public bool completable;
    public string title;
    public string desc;
    public int ID;
    public int Reward;
    public List<QuestGoal> goals;
    public int progress = 0;

    public void comepletequest()

    {
        int numcomplete = 0;
        foreach(QuestGoal g in goals)
        {
            if (g.completed) { numcomplete++; }

        }
        if(numcomplete == goals.Count)
        {
            completable = true;
        }
    }
    }
[System.Serializable]
public class QuestGoal
{
    public string description;
    public int requireditem;
    public int requiredAmount;
    public int requiredBool;

    public InventoryChecker checker;
    public enum QuestType
    {
        Gather
  ,     Dialogue
    }
   
    
    public QuestType questtype;
    public bool completed;

  
}


