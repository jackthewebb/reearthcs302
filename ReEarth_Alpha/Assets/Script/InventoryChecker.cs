﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestInventory;

public class InventoryChecker : MonoBehaviour
{
    public List<Item> backpack;
    private Inventory inventoryRef;
    public BackpackUI BPUI;
    public PickupText pickuptext;
    public Transform pickupcanvas;

    void Start()
    {
        BPUI = FindObjectOfType<BackpackUI>();
        GetResources();

    }

    void Update()
    {

    }
    void GetResources()
    {

        inventoryRef = (Inventory)Resources.Load("Inventories/Inventory");

    }
    public Item FindItems(int ItemType)
    {
        Item item = null;
        switch (ItemType)
        {
            case 1:
                item = inventoryRef.rock;
                break;
            case 2:
                item = inventoryRef.wood;
                break;
            case 3:
                item = inventoryRef.Ball;
                break;
            case 4:
                item = inventoryRef.BlueFlower;
                break;

                

        }
        return item;

    }

    public void AddtoInv(int type, int amount)
    {

        Item ItemtoAdd = FindItems(type);
        ItemtoAdd.quantity += amount;


        PickupText newText = Instantiate(pickuptext, pickupcanvas);
        newText.DisplayText(amount, ItemtoAdd.name.ToString());



    }
    public bool GatherQuestCheck(int type, int amount)
    {    

        Item ItemtoAdd = FindItems(type);
        if (ItemtoAdd.quantity >= amount)
        {

            return true;
           
        }

        else { return false; }

    }

    public void BackPackDisplayer()
    {
       List<Item> backpack = new List<Item> { inventoryRef.rock, inventoryRef.wood,inventoryRef.Ball, inventoryRef.BlueFlower};

        foreach (Item i in backpack)
        {
            
            int amount = i.quantity;

            BPUI.SpawnBPthing(i.name, amount);

        }

    }

}
