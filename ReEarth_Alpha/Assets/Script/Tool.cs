﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{

    public int toolnum;
    public bool visibility;
    public int ToolType;
    // type 1 = pickaxe 2 = axe 3 = spade 4 = fishingrod
    private MeshRenderer renderer;
    


    // Start is called before the first frame update
    void Start()
    {
           renderer = GetComponent<MeshRenderer>();



    }

    // Update is called once per frame
    void Update()
    {


        if (visibility == false)
        {
            renderer.enabled = false;
        }
        else
        {

            renderer.enabled = true;

        }

    }

}
