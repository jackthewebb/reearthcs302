﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Door1 : MonoBehaviour

{
	public int DoorNumber = 0;
	public Player player;


    // Start is called before the first frame update
    private void OnLevelWasLoaded(int level)
    {
		player = FindObjectOfType<Player>();

	}

	 public void GoThoughDoor() {
		player = FindObjectOfType<Player>();

		player.doorTicket = DoorNumber;

		switch (DoorNumber){
			case 0:
			SceneManager.LoadScene("Main");
			break;
			
			case 1:
			SceneManager.LoadScene("Inside1");
			break;

			case 2:
				SceneManager.LoadScene("Main");
				break;

		}

	 }
}
