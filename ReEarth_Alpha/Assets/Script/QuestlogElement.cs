﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestlogElement : MonoBehaviour
{
    public int id;
    public Quest quest;
    public string Questname;
    public string Description;
    public QuestlogUI myparent;
    public Text NameText;
    public Text DescriptionText;

    private void Start()
    {

        NameText.text = quest.title;
    }
    public void displayQuest()
    {

        myparent.DisplayQuestData(quest);
    }
}
