﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class DialogueBox : MonoBehaviour
{
    public Text Option1;
    public Text Option2;
    public Text Option3;
    public Text textRef;
    public Text nameRef;
    public Text Option4;
    public CanvasGroup canvasGroup;
    public CanvasGroup canvasGroupOptions;
    public Image DisplayImage;
    public bool PlayerDeciding;
    public PlayerMovement playerM;
    public bool talking;
    public int BroadcastDecision;
    public Player player;
    public bool animatingtext = false;
    string currentline;
    private int PlayerDecision;
    private Queue<string> sentences = new Queue<string>();
    private Queue<string> names = new Queue<string>();
    private Queue<string> sprites = new Queue<string>();
    private IEnumerator coroutine;
    private int r;
    

    private void OnEnable() {
        SceneManager.sceneLoaded += OnLoad;
    }

    private void OnDestroy() {
        SceneManager.sceneLoaded -= OnLoad;
    }

    private void OnLoad(Scene _scene, LoadSceneMode _loadSceneMode) {
       
        playerM = FindObjectOfType<PlayerMovement>();
    }

    void Start() {
        playerM = FindObjectOfType<PlayerMovement>();
        HideBox();
        player = FindObjectOfType<Player>();
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {

            if (PlayerDeciding == false)
            {
                if (animatingtext == true)
                {
                    animatingtext = false;
                    StopCoroutine(coroutine);
                    textRef.text = currentline;
                }
                else
                {
                    if (sentences.Count != 0)
                    {

                        DisplayDialogue();
                    }
                    else
                    {
                        StopTalking();
                    }
                }
            }

        }
        GetInput();

    }

    public void StartTalking()
    {
        player.canMenu = false;
        ShowBox();
        playerM.canmove = false;
        talking = true;
        ThirdPersonCamera cam = FindObjectOfType<ThirdPersonCamera>();
        cam.canrotate = false;
    }

    public void StopTalking()
    {
        player.canMenu = true;
        HideBox();
        playerM.canmove = true;
        talking = false;
        ThirdPersonCamera cam = FindObjectOfType<ThirdPersonCamera>();
        cam.canrotate = true;

    }


    public void ShowBox()
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;
    }
    public void ShowOptions()
    {
        canvasGroupOptions.alpha = 1f;
        canvasGroupOptions.blocksRaycasts = true;
    }
    public void HideOptions()
    {
        canvasGroupOptions.alpha = 0f;
        canvasGroupOptions.blocksRaycasts = false;
    }
    public void HideBox()
    {
        canvasGroup.alpha = 0f;
        canvasGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void ObtainDialogue(Queue<List<string>> Dialogue)
    {
        if (Dialogue != null)
        {
            sentences.Clear();
            names.Clear();
            textRef.text = "";
            textRef.name = "";

            foreach (List<string> x in Dialogue)
            {
                sentences.Enqueue(x[0]);
                names.Enqueue(x[1]);
                sprites.Enqueue(x[2]);
            }

            DisplayDialogue();
        }
    }

    public void DisplayDialogue()
    {
        ShowBox();
        //Nametext
        nameRef.text = names.Dequeue();
        //textRef.text =

            //contextText
            //string currentText = ;
            string text = sentences.Dequeue();
            coroutine = AnimateText(text);





            StartCoroutine(coroutine);

        

        string strSpr = sprites.Dequeue();
        // Unpack sprite.
        Sprite spr = Resources.Load<Sprite>("Textures/Avatars/" + strSpr);
        DisplayImage.sprite = spr;
    }
    public void PlayerInput(List<string> options)
    {
        PlayerDeciding = true;
    }

    public IEnumerator ChoosingBox(List<string> choices)
    {
        ShowOptions();
        Option1.text = "1) " + choices[0];
        Option2.text = "2) "+choices[1];
        Option3.text = "3) "+choices[2];
        Option4.text = "4) "+choices[3];
        PlayerDeciding = true;
        yield return new WaitUntil(() => PlayerDecision > 0);
        PlayerDeciding = false;
        BroadcastDecision = PlayerDecision;
        HideOptions();
        r = 0;
        PlayerDecision = 0;
      
        yield return null;
    }
    public void GetInput()
    {
       
        if (PlayerDeciding)
        {
            
                if (Input.GetKeyDown("1"))
                {
                    r = 1;
                }
                if (Input.GetKeyDown("2"))
                {
                    r = 2;
                }
                if (Input.GetKeyDown("3"))
                {
                    r = 3;
                }
                if (Input.GetKeyDown("4"))
                {
                    r = 4;
                }
            
            
        }
        PlayerDecision = r;
    }


    IEnumerator AnimateText(string text)
    {

        animatingtext = true;
        currentline = text;
        for (int i = 0; i < (text.Length + 1); i++)
        {
            textRef.text = text.Substring(0, i);
            yield return new WaitForSeconds(.03f);
            
        }
        animatingtext = false;

    }

    public void Input1()
    {
        if (PlayerDeciding)
        { r = 1; }
    }
    public void Input2()
    {
        if (PlayerDeciding)
        { r = 2; }
    }

    public void Input3()
    {
        if (PlayerDeciding)
        { r = 3; }
    }

    public void Input4()
    {
        if (PlayerDeciding)
        { r = 4; }
    
}






}
