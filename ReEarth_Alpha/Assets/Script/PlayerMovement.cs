﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    CharacterController characterController;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Animator anim;
    public bool canmove = true;

    private float CurrentSpeed;
    private Vector3 moveDirection = Vector3.zero;


    void Start() {
        characterController = FindObjectOfType<CharacterController>();
        if (SceneManager.GetActiveScene().name == "PlayerCreation")
        {
            canmove = false;
        }
        else
        {
            canmove = true;
        }




    }
    private void OnLevelWasLoaded(int level)
    {

        if (SceneManager.GetActiveScene().name == "PlayerCreation")
        {

            canmove = false;
            Debug.Log("lmao wat");
        }
        else
        {
            canmove = true;
        }


    }






    void Update() {

        if (canmove) { Move(); }
        else{ CurrentSpeed = 0; }
       
        anim.SetFloat("Speed", CurrentSpeed);
        anim.SetBool("CanMove", canmove);

       
        
    }

    private void Move() {
        if (characterController.isGrounded) {
            // We are grounded, so recalculate
            // move direction directly from axes
            Transform cameraRig = ThirdPersonCamera.Instance.transform;
            Vector3 right = cameraRig.right;
            Vector3 forward = cameraRig.forward;

            Vector2 inputDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            Vector3 force = ((inputDirection.x * right) + (inputDirection.y * forward));
            force.y = 0;
            CurrentSpeed = force.magnitude;
            moveDirection = force;
            moveDirection *= speed;

            if (!Input.GetKey("left alt")) {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, cameraRig.rotation, Time.deltaTime * 900);
            }

            if (Input.GetButton("left shift")) {
                anim.SetBool("Sprinting", true);

                speed = 10f;
            }
            else
            {
                anim.SetBool("Sprinting", false);

                speed = 6f;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);
    }

}
