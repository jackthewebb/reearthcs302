﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public CanvasGroup InGameMenu;
    public CanvasGroup MenuButtons;
    public CanvasGroup OptionsMenu;
    public CanvasGroup QuestLog;
    public CanvasGroup Backpack;
    public InventoryChecker checker;
    public QuestlogUI Quester;
    public CanvasGroup CurrentUI = null;
    public bool Open = false;
    public Player player;

    public BackpackUI BPUI;

    private void Start()
    {
        getrefs();

        HideUI(MenuButtons);
        HideUI(Backpack);
        HideUI(QuestLog);
        HideUI(InGameMenu);
        CloseMenu();

    }
    void getrefs()
    {
        BPUI = FindObjectOfType<BackpackUI>();
        player = FindObjectOfType<Player>();
        checker = FindObjectOfType<InventoryChecker>();
    }
    private void OnLevelWasLoaded(int level)
    {
        getrefs();
       
    }
    public void OpenMenu()
    {
        Open = true;
        InGameMenu.alpha = 1f;
        HideUI(Backpack);
        ShowUI(InGameMenu);
        ShowUI(MenuButtons);
        CurrentUI = InGameMenu;
        player.ExitGameplayState();

    }
    public void CloseMenu()
    {
        if(CurrentUI == InGameMenu)
        {
        Open = false;
        HideUI(InGameMenu);
            player.EnterGameplayState();
        }
        else
        {
            HideUI(CurrentUI);
            ShowUI(MenuButtons);
            CurrentUI = InGameMenu;  
            
        }  
    }

    public void Showbackpack()
    {

        ShowUI(Backpack);
        BPUI.destroyall();
        checker.BackPackDisplayer();
        HideMenuButtons();

    }

    public void ShowOptions()
    {
        HideUI(MenuButtons);
        ShowUI(OptionsMenu);

    }
    public void ShowQuestlog()
    {
        HideUI(MenuButtons);
        Quester.destroyall();
        player.QuestLogDisplayer();
        ShowUI(QuestLog);
    }
    void HideMenuButtons()
    {


        MenuButtons.alpha = 0f;

    }
    public void ShowUI(CanvasGroup ui)
    {

        ui.alpha = 1f;
        CurrentUI = ui;
        ui.blocksRaycasts = true;
        ui.interactable = true;


    }
    void HideUI(CanvasGroup ui)
    {
        ui.alpha = 0f;
        ui.interactable = false;
        ui.blocksRaycasts = false;

    }



}
