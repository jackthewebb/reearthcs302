﻿using System;
using UnityEngine;

namespace TestInventory {
    [Serializable]
    public class Item {
        public string name;
        public int quantity = 0;
        [TextArea]
        public string description = "None";

        public void Add(int amount) {
            quantity += amount;
        }
    }
}
