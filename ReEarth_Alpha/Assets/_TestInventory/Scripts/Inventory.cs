﻿using UnityEngine;

namespace TestInventory {
    [CreateAssetMenu(fileName = "New Inventory", menuName = "Custom/TestInventory/Inventory")]
    public class Inventory : ScriptableObject {
        public Item rock;
        public Item wood;
        public Item doll;
        public Item BlueFlower;
        public Item Ball;

    }
    
}
