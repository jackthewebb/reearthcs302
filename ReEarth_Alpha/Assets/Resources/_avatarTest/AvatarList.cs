﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "newavatarlist", menuName = "Custom/AvatarList")]
public class AvatarList : ScriptableObject {
    [Serializable]
    public class AvatarSet {
        [Serializable]
        public class Emotions {
            public Sprite happy;
            public Sprite sad;
            public Sprite disappointed;
            public Sprite scared;
            public Sprite confuse;
            public Sprite shock;
            public Sprite angry;
            public Sprite normal;

        }

        public string name;
        public Emotions emotions;
    }

    public AvatarSet bobby;
    public AvatarSet Player;
    public AvatarSet Isana;
    public AvatarSet Mozark;
    public AvatarSet Sage;
    public AvatarSet June;
    public AvatarSet Angel;
    public AvatarSet Genesis;
    public AvatarSet Coco;








}
